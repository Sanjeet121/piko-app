export class PKConstants{
    
    // ----------------- HTTP CONSTANTS --------------------//
    public static readonly REQUEST_GET: string = "GET";
    public static readonly REQUEST_POST: string = "POST";
    public static readonly REQUEST_PUT: string = "PUT";
    public static readonly API_ERROR:string = "apiError";

//------------------ API URL ----------------------- //
public static GET_TEST_DATA_URL : string = "";
public static GOOGLE_PLACES_API_URL : string = "";
public static AWS_S3_OBJECT_URL: string = "";
public static GET_LOG_IN_DATA : string = "";

// ----------------- Result Type ------------------- //
public static readonly RESULT_GET_TEST_DATA : string = "resultGetTestData";
public static readonly RESULT_GET_LOG_IN_DATA : string = "resultGetLogInData";

    // ------------ Bucket name --------//
  public static readonly BUCKET_TYPE_PROFILE_DOC : string = "profiledoc";



  // ----- regex -----//
  public static readonly REGEX_TYPE_NAME : string = "([a-zA-Z',.-]+( [a-zA-Z',.-]+)*){2,30}";
  public static readonly REGEX_TYPE_NUMBER : string = "^[0-9]*$";
  public static readonly REGEX_TYPE_AMOUNT : string = '^[0-9]?((\.[0-9]+)|([0-9]+(\.[0-9]+)?))$';



  // --------------------- Others --------------------//
  public static readonly KEY_ACCOUNT_HASH_ID: string = "userKeyAccountHashId";
  public static readonly KEY_MOBILE : string = "phoneNumber";
  public static readonly KEY_USER_ID : string = "userId";
  public static readonly KEY_PASSWORD : string = "password";
  public static readonly KEY_AUTH_ID : string = "awsHashId";
  public static readonly KEY_USER_TYPE : string = "userType";
  public static readonly KEY_SELECTED_ZONE_HASH_ID : string = "userSelectedZoneHashId";
  public static readonly KEY_SELECTED_ZONE_NAME : string = "userSelectedZoneName";
  public static readonly KEY_SELECTED_FORMATTED_ADDRESS : string = "selectedFormattedAddress";
  public static readonly KEY_SELECTED_LOCATION : string = "selectedLocation";
  public static readonly KEY_SELECTED_LATITUDE : string = "selectedLatitude";
  public static readonly KEY_SELECTED_LONGITUDE : string = "selectedLongitude";
  public static readonly KEY_SELECTED_CART_ITEM : string = "userSelectedCartInventoryItem";
  public static readonly KEY_ITEM_CART_BELL_STATUS : string = "itemCartBellStatus";
  public static readonly KEY_USER_IMAGE : string = "userImageUrl";
  public static readonly KEY_USER_NAME : string = "userName";
  public static readonly KEY_USER_ADDRESS : string = "userAddress";
  public static readonly KEY_SELECTED_THEME : string = "selectedTheme";
  /**User Type */
  public static readonly USER_TYPE_ADMIN : string = "0";
  public static readonly USER_TYPE_SUB_ADMIN : string = "2";
  public static readonly USER_TYPE_NORMAL_USER : string = "1";
  public static readonly USER_TYPE_CALL_CENTER : string= '3';
  public static readonly USER_TYPE_CITY_MANAGER : string= '4';
  public static readonly USER_TYPE_DRIVER : string= '5';
  public static readonly USER_TYPE_DELIVERY_PERSON : string= '6';
  public static readonly USER_TYPE_FLEET_MANAGER : string= '7';
  public static readonly USER_TYPE_MERCHANT : string= '8';
  public static readonly USER_TYPE_LAUNDRY : string= '9';
  public static readonly USER_SERVICE_PERSONNEL : string= '10';

//----Order status---//
public static readonly ORDER_STATUS_ORDER_PLACED_BY_USER : string= "order_placed_by_user";
public static readonly ORDER_STATUS_PAYMENT_DONE_FOR_THE_ORDER : string = "payment_done_for_the_order";
public static readonly ORDER_STATUS_ORDER_PLACED_BY_CALL_CENTER_OR_ADMIN : string = "order_placed_by_call_center_or_admin";
public static readonly ORDER_STATUS_ORDER_CANCELLED_BY_USER : string = "order_cancelled_by_user";
public static readonly ORDER_STATUS_ORDER_CANCELLED_BY_CALL_CENTER_OR_ADMIN : string = "order_cancelled_by_call_center_or_admin";
public static readonly ORDER_STATUS_ORDER_CANCELLED_BY_MERCHANT : string = "order_cancelled_by_merchant";
public static readonly ORDER_STATUS_ORDER_ACCEPTED_BY_MERCHANT : string = "order_accepted_by_merchant";
public static readonly ORDER_STATUS_ORDER_CANCELLED_BY_MERCHANT_AFTER_ACCEPTING : string= "order_cancelled_by_merchant_after_accepting";
public static readonly ORDER_STATUS_ORDER_READY_BY_MERCHANT : string = "order_ready_by_merchant";
public static readonly ORDER_STATUS_ORDER_WILL_BE_DELAYED_BY_MERCHANT : string = "order_will_be_delayed_by_merchant";
public static readonly ORDER_STATUS_DELIVERY_ASSIGNED_TO_DELIVERY_BOY : string = "delivery_assigned_to_delivery_boy";
public static readonly ORDER_STATUS_ORDER_PICKED_UP_BY_DELIVERY_BOY : string = "order_picked_up_by_delivery_boy";
public static readonly ORDER_STATUS_DELIVERY_WILL_BE_DELAYED_BY_DELAYED_BY_DELIVERY_BOY : string = "delivery_will_be_delayed_by_delivery_boy";
public static readonly ORDER_STATUS_DELIVERY_BOY_REACHED_AT_DROP_LOCATION : string = "delivery_boy_reached_at_drop_location";
public static readonly ORDER_STATUS_DELIVERY_COMPLETE  :string = "delivery_complete";
public static readonly ORDER_STATUS_USER_RATING_DONE_FOR_THE_ORDER : string = "user_rating_done_for_the_order";
public static readonly ORDER_STATUS_USER_RATING_DONE_FOR_THE_DELIVERY : string = "user_rating_done_for_the_delivery";
public static readonly ORDER_STATUS_ORDER_ACCEPTED_BY_DELIVERY_BOY : string = "order_accepted_by_delivery_boy";
public static readonly ORDER_STATUS_DELIVERY_CANCEL_BY_USER : string = "delivery_cancel_by_user";
public static readonly ORDER_MAXIMUM_DISTANCE_KM : number = 10;
//------------------Sub service Type-------------------//

public static readonly SUB_SERVICE_TYPE_GROCERY : string = "1";
public static readonly SUB_SERVICE_TYPE_RESTAURANT : string = "2";
public static readonly SUB_SERVICE_TYPE_COSMETIC : string = "3";
public static readonly SUB_SERVICE_TYPE_COURIER : string = "4";
public static readonly SUB_SERVICE_TYPE_LAUNDARY : string = "5";
public static readonly SUB_SERVICE_TYPE_PAN : string = "6";
public static readonly SUB_SERVICE_TYPE_VEGETABLE : string = "7";
public static readonly SUB_SERVICE_TYPE_STREET_ORDER : string = "8";
public static readonly SUB_SERVICE_TYPE_CAB_ORDER : string = "9";
public static readonly SUB_SERVICE_TYPE_STATIONARY_ORDER : string = "10";
public static readonly SUB_SERVICE_TYPE_SERVICE_ORDER : string = "11";
public static readonly SUB_SERVICE_TYPE_PERISHABLE_ORDER : string = "12";
public static readonly SUB_SERVICE_TYPE_MEDICINE : string = "13";
public static readonly SUB_SERVICE_TYPE_MEAT_AND_FISH : string = "14";
public static readonly SUB_SERVICE_TYPE_BIKE_ORDER : string = "16";
public static readonly SUB_SERVICE_TYPE_AUTO_ORDER : string = "15";
public static readonly SUB_SERVICE_TYPE_TOTO_ORDER : string = "17";
public static readonly SUB_SERVICE_TYPE_UTILITY_SERVICE : string = "18";
/**
 * End
 */

}