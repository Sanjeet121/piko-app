import { Injectable } from '@angular/core';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { WebView } from '@ionic-native/ionic-webview/ngx';
import { File } from '@ionic-native/file/ngx';
import { ActionSheetController, LoadingController } from '@ionic/angular';
import { CmStorageService } from './cm-storage.service';
import { ErrorService } from './error.service';
import { ApiService } from './api.service';
import { UploadService } from './upload.service';
import { PKConstants } from '../constant/pkConstant';

@Injectable({
  providedIn: 'root'
})
export class UploadImageService {
  private readonly PHOTO_CAMERA: string = 'CAMERA';
  private readonly PHOTO_GALLERY: string = 'PHOTOLIBRARY';
  webViewFileSrc: string;
  constructor(private camera: Camera, 
    private file: File,
    private webview: WebView,
    private actionSheetController: ActionSheetController, 
    private loadingController: LoadingController,
    private cmStorage: CmStorageService, 
    private errorService: ErrorService,
    private apiService: ApiService,
    private uploadService : UploadService) { 

    }

     async presentOptionList() {
      const actionSheet = await this.actionSheetController.create({
        header: 'Choose option',
        buttons: [{
          text: 'Choose from Gallery',
          icon: 'image-outline',
          handler: () => {
            console.log('Gallery clicked');
            this.getPictureFromDevice(this.PHOTO_GALLERY);
          }
        }, 
         {
          text: 'Use camera',
          icon: 'camera-outline',
          handler: () => {
            this.getPictureFromDevice(this.PHOTO_CAMERA);
            console.log('Camera clicked');
          }
        }, {
          text: 'Cancel',
          icon: 'close',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }]
      });
      await actionSheet.present();
    }
    private getPictureFromDevice(source: string){
      const options: CameraOptions = {
        quality: 60,
        correctOrientation: true,
        destinationType: this.camera.DestinationType.FILE_URI,
        encodingType: this.camera.EncodingType.JPEG,
        mediaType: this.camera.MediaType.PICTURE,
        sourceType: this.camera.PictureSourceType[`${source}`]
      }
  
      this.camera.getPicture(options).then(imageData =>{
        console.log('response from get picture\n', imageData);
        this.webViewFileSrc = this.convertFilePath(imageData);
        this.showLoader('Uploading').then(()=>{
          this.convertToBlob(imageData).then(blob =>{
            console.log(blob);
            this.uploadService.uploadToS3(PKConstants.BUCKET_TYPE_PROFILE_DOC,this.cmStorage.getValueFromStorage(PKConstants.KEY_ACCOUNT_HASH_ID)+'/image','profile_picture',blob,this.webViewFileSrc).then(result=>{
              console.log(result);
              this.hideLoader();
            })
          }).catch(error =>{
            console.log(error);
            this.errorService.errorHandler({message: 'Error reading file, unsupported file format.'}, 'Upload error');
  
          });
        });
      }).catch(error =>{
        console.log(error);
        this.errorService.errorHandler({message: error},'Upload error');
      });
    }
    private convertFilePath(fileUri: string){
      const viewPath = this.webview.convertFileSrc(fileUri);
      return viewPath;
    }
    private async convertToBlob(filepath: string): Promise<Blob>{
      const entry = await this.file.resolveLocalFilesystemUrl(filepath).catch((err) =>{
        console.log(err);
        return Promise.reject(err);
      });
      // console.log(entry);
      if(entry){
        let tempPath = entry.nativeURL.substr(0, entry.nativeURL.lastIndexOf('/') + 1);
        let tempName = entry.name;
        // console.log(tempName + '\n' + tempPath);
        const arrayBuffer = await this.file.readAsArrayBuffer(tempPath, tempName);
        const blobData = new Blob([arrayBuffer], {
          type: 'image/png'
        });
        return blobData;
      }
    }
    async showLoader(msg: string) {
      const loading = await this.loadingController.create({
        spinner: "crescent",
        message: msg,
      });
      await loading.present();
    }
  
    hideLoader(){
      this.loadingController.dismiss();
    }

/**
 * End
 */

}
