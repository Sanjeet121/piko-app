import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import { AlertController, LoadingController } from '@ionic/angular';

import { Subject } from 'rxjs';
import { ErrorCodes } from '../constant/error-messages.enum';

@Injectable({
  providedIn: 'root'
})
export class ErrorService{

  errorMessage: string;
  errorCode: string;
  errorHeading: string;
  errorButtonText: string = 'OK';
  verify : Subject<boolean> = new Subject<boolean>();

  constructor(private alertController: AlertController, private router: Router, private loadingController: LoadingController){}

  errorHandler(error: { code?: any; message?: any; }, header?: string){

    this.loadingController.getTop().then(loader =>{
      console.log('loading ctrl', loader);
      if(loader){
        this.loadingController.dismiss();
      }
    });

    switch (error.code) {

      case ErrorCodes.AWS_USER_WRONG_CREDENTIALS:
        this.errorButtonText = 'Okay';
        break;
      
      case ErrorCodes.AWS_USER_NOT_CONFIRMED:
        this.errorButtonText = 'Okay';
        break;

      case ErrorCodes.AWS_INVALID_VALIDATION:
        error.message = 'Your credentials are either not properly validated or doesn\'t satisfy minimum requirements';

      default:
        this.errorButtonText = 'Okay';
        break;
    }
    this.errorAlert(error, header);
  }

  
  async errorAlert(error: { code?: any; message?: any; }, header: string) {
    const alert = await this.alertController.create({
      header: header ? header : undefined,
      message: error.message,
      buttons: [{
          text: this.errorButtonText,
          handler: () => {
            if(this.errorButtonText === 'verify'){
              this.verify.next(true);
            } else if(this.errorButtonText === 'login'){
            } else{
              return null;
            }
           
          }
        }
      ]
    });

    await alert.present();
  }
  
}
