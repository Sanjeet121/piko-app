import { Injectable, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs/internal/Subscription';
import { ApiService } from './api.service';
import { Router } from '@angular/router';
import { ErrorService } from './error.service';
import { ToastController, LoadingController, NavController } from '@ionic/angular';
import { CmStorageService } from './cm-storage.service';
import { Auth } from 'aws-amplify';
@Injectable({
  providedIn: 'root'
})
export class AuthService implements  OnInit, OnDestroy{
  private _apiSubscription:Subscription;
  constructor(private router: Router,
    private errorService: ErrorService, 
    private apiService: ApiService, 
    private toastController: ToastController, 
    private loadingController: LoadingController,
    private cmStorage: CmStorageService,
    private navController : NavController,
   ) { 
    this._apiSubscription = this.apiService.apiResults.subscribe(data =>{
      if(data){

      }
    });
   }
   ngOnInit(){
  }
  ngOnDestroy(){
    this._apiSubscription.unsubscribe();
  }
    userSignIn(userId : string , password : string):Promise<any>{
      return new Promise((resolve,reject)=>{
        this.showLoader("Logging in").then(()=>{
          Auth.signIn(userId,password).then(result=>{
            this.hideLoader();
            resolve(result);
          }).catch(error=>{
            this.hideLoader();
            reject(error);
          })
        })
      })
    }
    changePasswordForcefully(user,password,username):Promise<any>{
      return new Promise((resolve,reject)=>{
        this.showLoader("Changing password").then(()=>{
          Auth.completeNewPassword(user ,password,{
            preferred_username : username
          }).then(res=>{
            this.hideLoader();
            resolve(res);
          }).catch(err=>{
            this.hideLoader();
            reject(err);
          })
        });
      });
    }

    userLogout():Promise<any>{
      return new Promise((resolve,reject)=>{
       this.showLoader("Log out").then(()=>{
        Auth.signOut().then(res=>{
          this.hideLoader();
          resolve(res)
        }).catch(err=>{
          this.hideLoader();
          reject(err);
        });
       });
      });
    }

    changePassword(newPassword:string , oldPassword : string):Promise<any>{
      return new Promise((resolve,reject)=>{
        this.showLoader("Changing Password").then(()=>{
          Auth.currentAuthenticatedUser().then(result=>{
            console.log("Current authentic user: ",result)
            Auth.changePassword(result,oldPassword,newPassword).then(value=>{
              console.log(value);
              this.hideLoader();
              resolve(value);
            }).catch(err=>{
              console.log(err);
              this.hideLoader();
              reject(err);
            })
          }).catch(err=>{
            console.log(err);
            this.hideLoader();
          })
        })
       
      })
    }


    async successToast(msg: string) {
      const toast = await this.toastController.create({
        message: msg,
        color: "success",
        duration: 2000
      });
      toast.present();
    }
  
    async showLoader(msg: string) {
      const loading = await this.loadingController.create({
        message: msg,
        spinner: "bubbles"
      });
      await loading.present();
    }
  
    hideLoader(){
      this.loadingController.dismiss();
    }
/**
 * End
 */

}
