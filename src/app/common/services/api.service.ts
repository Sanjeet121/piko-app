import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { CmStorageService } from './cm-storage.service';
import { HeaderParam } from '../utils/header-param';
import { ApiParam } from '../utils/api-param';
import { PKConstants } from '../constant/pkConstant';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  private _result:Subject<{resulttype: string, result: any, inputParams? : any}> = new Subject<{resulttype: string, result: any, inputParams? : any}>();
  constructor(
    private http: HttpClient, 
    private cmStorage : CmStorageService
    ){}

    


    private genericApiCall(apiparam: ApiParam, htpHeader: any = undefined) {
      
      switch (apiparam.requesttype) {
        case PKConstants.REQUEST_GET:
          console.log(apiparam,htpHeader);
          this.http.get(apiparam.url, htpHeader).subscribe(
            (data) => {
              console.log("GET REQUEST MADE", data);
             
              
              if (!this.validResultCode(data, apiparam.resulttype)) {
                this._result.next({
                  resulttype: apiparam.resulttype,
                  result: data,
                  inputParams: apiparam.inputParams,
                });
              }
            },
            (error) => {
              
              // console.log(error);
              // this.handleError(error);
            }
          );
          break;
  
        case PKConstants.REQUEST_POST:
          this.http
            .post(apiparam.url, apiparam.bodydata, htpHeader)
            .subscribe(
              (data) => {
                if (!this.validResultCode(data, apiparam.resulttype)) {
                  this._result.next({
                    resulttype: apiparam.resulttype,
                    result: data,
                    inputParams: apiparam.inputParams,
                  });
                }
                
              },
              (error) => {
                // this.handleError(error);
              }
            );
          break;
  
        case PKConstants.REQUEST_PUT:
          this.http.put(apiparam.url, apiparam.bodydata, htpHeader).subscribe(
            (data) => {
              if (!this.validResultCode(data, apiparam.resulttype)) {
                this._result.next({
                  resulttype: apiparam.resulttype,
                  result: data,
                  inputParams: apiparam.inputParams,
                });
              }
            },
            (error) => {
              // this.handleError(error);
            }
          );
  
          break;
      }
    }
    private getHeaderInfo(headerlist: Array<HeaderParam>): any {
      let htpheaders = new HttpHeaders();
      for (let hp of headerlist) {
        htpheaders = htpheaders.set(hp.keyName, hp.keyValue);
      }
  
      const httpOpt = {
        headers: htpheaders,
      };
      return httpOpt;
    }
    private validResultCode(data: any, result_type: string): boolean {
      return false;
    }
  
    get apiResults() {
      return this._result.asObservable();
    }
  }
