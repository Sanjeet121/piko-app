import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { PKConstants } from '../constant/pkConstant';
import { Storage } from '@ionic/storage';

@Injectable({
  providedIn: 'root'
})
export class ExitGuard implements CanActivate {
  constructor(private router: Router, private storage: Storage){}
  
  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

    // console.log(next, state);
   
    return this.storage.get(PKConstants.KEY_ACCOUNT_HASH_ID).then(value =>{
       console.log(value);

      if(value === null || value === undefined){
        this.router.navigate(['login']);
        return false;
      } else{
        return true;
      }
    });
  } 
  
}
