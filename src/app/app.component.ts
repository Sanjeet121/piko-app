import { Component, ViewChild } from '@angular/core';
import { IonRouterOutlet, Platform, AlertController } from '@ionic/angular';
import { UrlConverterService } from './common/services/urlconverter.service';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { PKConstants } from './common/constant/pkConstant';
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  @ViewChild(IonRouterOutlet, {static: true}) routerOutlet: IonRouterOutlet;

  constructor(private _urlConverter:UrlConverterService,
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private alertController : AlertController
    ) {
      this.initializeApp();
    }
    toogleTheme(){
      if(localStorage.getItem(PKConstants.KEY_SELECTED_THEME) == 'light'){
        document.body.setAttribute('color-theme', 'light');
      }else if(localStorage.getItem(PKConstants.KEY_SELECTED_THEME) == 'dark'){
        document.body.setAttribute('color-theme', 'dark');
      }else{
        localStorage.setItem(PKConstants.KEY_SELECTED_THEME,'dark');
        document.body.setAttribute('color-theme', 'dark');
      }
    }
  initializeApp(){
    this.platform.ready().then(() => {
      this.toogleTheme();
      this.statusBar.overlaysWebView(false);
      this.statusBar.backgroundColorByHexString('#24ac34');
      this.splashScreen.hide();
      this._urlConverter.initialize();
      if(this.platform.is('android')){
        localStorage.setItem('platform', 'android');
      } else if(this.platform.is('ios')){
        localStorage.setItem('platform', 'ios');
      } else if(this.platform.is('mobileweb')){
        localStorage.setItem('platform', 'mobileweb');
      }   
      this.platform.backButton.subscribeWithPriority(-1 , () =>{
        if(!this.routerOutlet.canGoBack()){
          this.exitAppConfirmation();
        }
      });
    });
  }
  async exitAppConfirmation() {
    const alert = await this.alertController.create({
      header: 'Are you sure you want to exit?',
      buttons: [
        {
          text: 'No',
          role: 'cancel'
        }, {
          text: 'Yes',
          handler: () => {
            navigator['app'].exitApp();
          }
        }
      ]
    });
    await alert.present();
  }
}
